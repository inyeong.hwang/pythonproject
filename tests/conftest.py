import pytest
import mymath
import numpy as np


@pytest.fixture(scope="package")
def test_data():
    m1 = mymath.Matrix([[2, 3], [1, -4]])
    m2 = mymath.Matrix([[2, 3], [1, -3]])
    m3 = mymath.Matrix([[2, 3, 5], [1, -4, 2]])
    m4 = np.array([[2, 3], [1, -4]])
    return m1, m2, m3, m4

# @pytest.fixture
# def a(test_data):
#     a, _, _, _ = test_data
#     return a
#
# @pytest.fixture
# def b(test_data):
#     _, b, _, _ = test_data
#     return b
#
# @pytest.fixture
# def c(test_data):
#     _, _, c, _ = test_data
#     return c
#
# @pytest.fixture
# def d(test_data):
#     _, _, _, d = test_data
#     return d
