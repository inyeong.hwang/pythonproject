import mymath
from mymath import stats
import pytest
from pytest import approx
import numpy as np


def test_get_mean():
    assert mymath.get_mean([-1, 2, 5]) == approx(np.mean([-1, 2, 5]), rel=1e-3)
    assert stats.get_mean([1]) == approx(np.mean([1]), rel=1e-3)
    with pytest.raises(TypeError):
        assert mymath.get_mean(1)


def test_get_var():
    assert mymath.get_var([-1, 2, 5]) == approx(np.var([-1, 2, 5]), rel=1e-3)
    assert stats.get_var([1]) == approx(np.var([1]), rel=1e-3)
    with pytest.raises(TypeError):
        assert mymath.get_var(1)


def test_get_std():
    assert mymath.get_std([-1, 2, 5]) == approx(np.std([-1, 2, 5]), rel=1e-3)
    assert stats.get_std([1]) == approx(np.std([1]), rel=1e-3)
    with pytest.raises(TypeError):
        assert mymath.get_std(1)
