import mymath
from mymath import arithmetic
import pytest
from pytest import approx


def test_add():
    assert arithmetic.add(3, 1) == approx(4)
    assert mymath.add(-2, 3) == approx(1)


def test_sub():
    assert arithmetic.sub(3, 1) == approx(2)
    assert mymath.sub(-2, 3) == approx(-5)


def test_mult():
    assert arithmetic.mult(2, 4) == approx(8)
    assert mymath.mult(-2, 3) == approx(-6)


def test_div():
    assert arithmetic.div(2, 4) == approx(0.5)
    assert mymath.div(-4, 2) == approx(-2)
    with pytest.raises(ZeroDivisionError):
        assert mymath.div(4, 0)
