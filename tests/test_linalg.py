import numpy as np
import mymath
from mymath import linalg
import pytest


def test_madd(test_data):
    a, b, c, _ = test_data
    assert isinstance(mymath.madd(a, b), mymath.Matrix)
    assert np.array_equal((linalg.madd(a, b)).array, np.array([[4, 6], [2, -7]]))
    with pytest.raises(ValueError):
        assert mymath.madd(b, c)


def test_msub(test_data):
    a, b, c, _ = test_data
    assert isinstance(mymath.msub(a, b), mymath.Matrix)
    assert np.array_equal((linalg.msub(a, b)).array, np.array([[0, 0], [0, -1]]))
    with pytest.raises(ValueError):
        assert mymath.msub(b, c)


def test_mmult(test_data):
    a, b, c, _ = test_data
    assert isinstance(mymath.mmult(a, b), mymath.Matrix)
    assert np.array_equal((linalg.mmult(a, b)).array, np.array([[7, -3], [-2, 15]]))
    assert np.array_equal((mymath.mmult(b, c)).array, np.array([[7, -6, 16], [-1, 15, -1]]))
    with pytest.raises(ValueError):
        assert mymath.mmult(c, b)
