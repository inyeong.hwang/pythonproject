import mymath
from mymath import trigonometric
import math
from pytest import approx


def test_get_sin():
    assert mymath.get_sin(-30) == approx(math.sin(math.radians(-30)))
    assert trigonometric.get_sin(0) == approx(math.sin(math.radians(0)))
    assert mymath.get_sin(60) == approx(math.sin(math.radians(60)))


def test_get_cos():
    assert mymath.get_cos(-30) == approx(math.cos(math.radians(-30)))
    assert trigonometric.get_cos(0) == approx(math.cos(math.radians(0)))
    assert mymath.get_cos(60) == approx(math.cos(math.radians(60)))


def test_get_tan():
    assert mymath.get_tan(-30) == approx(math.tan(math.radians(-30)))
    assert trigonometric.get_tan(0) == approx(math.tan(math.radians(0)))
    assert mymath.get_tan(60) == approx(math.tan(math.radians(60)))
