import mymath
from mymath import nonlinear
import math
import pytest
from pytest import approx


def test_get_log():
    assert mymath.get_log(3) == approx(math.log(3))
    assert nonlinear.get_log(10.5) == approx(math.log(10.5))
    with pytest.raises(ValueError):
        assert mymath.get_log(0)
        assert mymath.get_log(-3)


def test_get_exp():
    assert mymath.get_exp(3) == approx(math.exp(3))
    assert nonlinear.get_exp(10.5) == approx(math.exp(10.5))
    assert mymath.get_exp(-3) == approx(math.exp(-3))
    assert mymath.get_exp(711) == approx(math.inf)


def test_pow():
    assert mymath.pow(2, 4) == approx(math.pow(2, 4), rel=1e-3)
    assert nonlinear.pow(0, 2) == approx(math.pow(0, 2), rel=1e-3)
    assert mymath.pow(4, -3) == approx(math.pow(4, -3), rel=1e-3)


def test_ceil():  # 올림
    assert mymath.ceil(3.7) == approx(math.ceil(3.7), rel=1e-3)
    assert nonlinear.ceil(3.2) == approx(math.ceil(3.2), rel=1e-3)
    assert mymath.ceil(-3.7) == approx(math.ceil(-3.7), rel=1e-3)
    assert mymath.ceil(3) == approx(math.ceil(3), rel=1e-3)


def test_floor():  # 내림
    assert mymath.floor(3.7) == approx(math.floor(3.7), rel=1e-3)
    assert nonlinear.floor(3.2) == approx(math.floor(3.2), rel=1e-3)
    assert mymath.floor(-3.7) == approx(math.floor(-3.7), rel=1e-3)
    assert mymath.floor(3) == approx(math.floor(3), rel=1e-3)


def test_round():  # 반올림
    assert mymath.round(3.7) == approx(round(3.7), rel=1e-3)
    assert nonlinear.round(3.2) == approx(round(3.2), rel=1e-3)
    assert mymath.round(-3.2) == approx(round(-3.2), rel=1e-3)
    assert mymath.round(-3.7) == approx(round(-3.7), rel=1e-3)
    assert mymath.round(3) == approx(round(3), rel=1e-3)
