import numpy as np
import mymath
import pytest


class TestMatrix:
    def test_init(self, test_data):
        a, _, c, _ = test_data
        assert isinstance(a, mymath.Matrix)
        assert isinstance(a.array, np.ndarray)
        assert c.dim == (2, 3)

    def test__add(self, test_data):
        a, b, _, d = test_data
        assert isinstance(b.add(a), np.ndarray)
        assert np.array_equal(b.add(a), np.array([[4, 6], [2, -7]]))
        with pytest.raises(TypeError):
            assert b.add(d)

    def test_sub(self, test_data):
        a, b, _, d = test_data
        assert isinstance(b.sub(a), np.ndarray)
        assert np.array_equal(b.sub(a), np.array([[0, 0], [0, 1]]))
        with pytest.raises(TypeError):
            assert b.sub(d)

    def test_mult(self, test_data):
        a, b, c, d = test_data
        assert isinstance(b.mult(a), np.ndarray)
        assert np.array_equal(b.mult(a), np.array([[7, -6], [-1, 15]]))
        with pytest.raises(TypeError):
            assert b.mult(d)
        with pytest.raises(ValueError):
            assert c.mult(b)
