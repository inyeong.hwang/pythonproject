import mymath
from mymath import const
from pytest import approx
import math


def test_get_pi():
    assert mymath.get_pi() == approx(math.pi, rel=1e-3)
    assert const.get_pi() == approx(math.pi, rel=1e-3)


def test_get_e():
    assert mymath.get_e() == approx(math.e, rel=1e-3)
    assert const.get_e() == approx(math.e, rel=1e-3)
