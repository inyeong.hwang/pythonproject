import mymath


def madd(a, b):
    if a.array.shape != b.array.shape:
        raise ValueError("오류: 행렬의 차원이 다름!")
    new_arr = a.array + b.array
    c = mymath.Matrix(new_arr)
    return c


def msub(a, b):
    if a.array.shape != b.array.shape:
        raise ValueError("오류: 행렬의 차원이 다름!")
    new_arr = a.array - b.array
    c = mymath.Matrix(new_arr)
    return c


def mmult(a, b):
    if a.array.shape[1] != b.array.shape[0]:
        raise ValueError("오류: 첫번째 행렬의 열 크기와 두번째 행렬의 행 크기가 다름!")
    new_arr = a.array.dot(b.array)
    c = mymath.Matrix(new_arr)
    return c
