def get_mean(li):
    if type(li) != list:
        raise TypeError("오류: 리스트 타입만 입력!")
    result = sum(li) / len(li)
    return result


def get_var(li):
    if type(li) != list:
        raise TypeError("오류: 리스트 타입만 입력!")
    dss = []
    for x in li:
        ds = (x - (sum(li) / len(li))) ** 2
        dss.append(ds)
    result = sum(dss) / len(dss)
    return result


def get_std(li):
    if type(li) != list:
        raise TypeError("오류: 리스트 타입만 입력!")
    dss = []
    for x in li:
        ds = (x - (sum(li) / len(li))) ** 2
        dss.append(ds)
    result = (sum(dss) / len(dss)) ** (1 / 2)
    return result
