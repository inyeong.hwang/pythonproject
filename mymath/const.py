def get_pi():
    n, limit, result = 0, 1000000, 0
    while True:
        result += (4 / (2 * n + 1)) * ((-1) ** n)
        n += 1
        if n == limit:
            break
    return result


def get_e():
    n, limit, result = 0, 1000000, 0
    while True:
        result = (1 + (1 / (n + 1))) ** n
        n += 1
        if n == limit:
            break
    return result
