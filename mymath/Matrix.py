import numpy as np
import mymath


class Matrix:
    def __init__(self, array):
        self.array = np.array(array)
        self.dim = self.array.shape

    def add(self, target):
        if isinstance(target, mymath.Matrix):
            result = mymath.madd(self, target)
            return result.array
        else:
            raise TypeError("오류: matrix 타입이 아님!")

    def sub(self, target):
        if isinstance(target, mymath.Matrix):
            result = mymath.msub(self, target)
            return result.array
        else:
            raise TypeError("오류: matrix 타입이 아님!")

    def mult(self, target):
        if isinstance(target, mymath.Matrix):
            result = mymath.mmult(self, target)
            return result.array
        else:
            raise TypeError("오류: matrix 타입이 아님!")
