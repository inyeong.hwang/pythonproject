import math


def get_log(a):
    return math.log(a)


def get_exp(a):
    try:
        return math.exp(a)
    except OverflowError:
        return math.inf


def pow(a, b):
    return a ** b


def ceil(a):  # 올림
    if a % 1 != 0:
        return (a // 1) + 1
    else:
        return a


def floor(a):  # 내림
    if a % 1 != 0:
        return a // 1
    else:
        return a


def round(a):  # 반올림
    if a % 1 >= 0.5:
        return (a // 1) + 1
    elif a % 1 < 0.5:
        return a // 1
    else:
        return a
