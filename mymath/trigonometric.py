import math


def get_sin(x):
    return math.sin(math.radians(x))


def get_cos(x):
    return math.cos(math.radians(x))


def get_tan(x):
    return math.tan(math.radians(x))
